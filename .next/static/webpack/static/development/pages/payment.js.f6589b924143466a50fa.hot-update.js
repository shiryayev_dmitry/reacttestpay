webpackHotUpdate("static\\development\\pages\\payment.js",{

/***/ "./pages/payment.tsx":
/*!***************************!*\
  !*** ./pages/payment.tsx ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _styles_styles_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../styles/styles.scss */ "./styles/styles.scss");
/* harmony import */ var _styles_styles_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_styles_styles_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _src_payment_Payment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../src/payment/Payment */ "./src/payment/Payment.tsx");
/* harmony import */ var _src_header_Header__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../src/header/Header */ "./src/header/Header.tsx");
var _jsxFileName = "C:\\Users\\dmitr\\Documents\\TestWeb\\my-app\\pages\\payment.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;



 //страница оплаты

const Payment = ({
  url: {
    query: {
      name
    }
  }
}) => __jsx("div", {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 8
  },
  __self: undefined
}, __jsx(_src_header_Header__WEBPACK_IMPORTED_MODULE_3__["default"], {
  text: "",
  link: true,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 10
  },
  __self: undefined
}), __jsx(_src_payment_Payment__WEBPACK_IMPORTED_MODULE_2__["default"], {
  name: name,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 11
  },
  __self: undefined
}));

/* harmony default export */ __webpack_exports__["default"] = (Payment);

/***/ })

})
//# sourceMappingURL=payment.js.f6589b924143466a50fa.hot-update.js.map