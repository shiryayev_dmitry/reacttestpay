webpackHotUpdate("static\\development\\pages\\payment.js",{

/***/ "./src/payment/Payment.tsx":
/*!*********************************!*\
  !*** ./src/payment/Payment.tsx ***!
  \*********************************/
/*! exports provided: Payment_view, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Payment_view", function() { return Payment_view; });
/* harmony import */ var _babel_runtime_corejs2_core_js_parse_float__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/parse-float */ "./node_modules/@babel/runtime-corejs2/core-js/parse-float.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_parse_float__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_parse_float__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _payment_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./payment.scss */ "./src/payment/payment.scss");
/* harmony import */ var _payment_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_payment_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_masked_text__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-masked-text */ "./node_modules/react-masked-text/dist/text-input-mask.js");
/* harmony import */ var react_masked_text__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_masked_text__WEBPACK_IMPORTED_MODULE_4__);

var _jsxFileName = "C:\\Users\\dmitr\\Documents\\TestWeb\\my-app\\src\\payment\\Payment.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;




function Payment_view({
  name
}) {
  const {
    0: money,
    1: setMoney
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(1);
  const {
    0: number,
    1: setNumber
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])();
  const {
    0: numberField,
    1: setnumberField
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])();

  function payPhone() {
    if (Math.random() >= 0.5) next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push('/index');else alert("Ошибка при отправке средств. Повторите позднее");
  }

  return __jsx("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, __jsx("form", {
    method: "POST",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, __jsx("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, name)), __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, __jsx("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, "\u041D\u043E\u043C\u0435\u0440 \u0442\u0435\u043B\u0435\u0444\u043E\u043D\u0430"), __jsx(react_masked_text__WEBPACK_IMPORTED_MODULE_4___default.a, {
    ref: ref => setnumberField(ref),
    kind: 'cel-phone',
    options: {
      withDDD: true,
      dddMask: '9 (999) 999-99-99'
    },
    value: number,
    onChangeText: text => {
      if (text === "8") text = "";
      if (text.length === 1) text = "8 " + text;
      if (text.length < 18) setNumber(text);
    },
    placeholder: "8 (999) 999-99-99",
    className: "form-control",
    required: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }), __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, numberField != null && numberField.isValid() ? "" : __jsx("div", {
    className: "error mb-2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }, "\u041D\u0435 \u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u043E \u0432\u0432\u0435\u0434\u0435\u043D\u043D\u044B\u0439 \u043D\u043E\u043C\u0435\u0440"))), __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 60
    },
    __self: this
  }, __jsx("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 61
    },
    __self: this
  }, "\u0421\u0443\u043C\u043C\u0430 \u043F\u043E\u043F\u043E\u043B\u043D\u0435\u043D\u0438\u044F"), __jsx(react_masked_text__WEBPACK_IMPORTED_MODULE_4___default.a, {
    kind: 'money',
    options: {
      maskedValue: 3,
      delimiter: ' ',
      unit: '',
      suffixUnit: 'RUB',
      zeroCents: true
    },
    value: money,
    onChangeText: text => {
      let index = text.indexOf('RUB') - 1;

      if (index <= 8) {
        setMoney(_babel_runtime_corejs2_core_js_parse_float__WEBPACK_IMPORTED_MODULE_0___default()(text.slice(0, index)));
      }
    },
    className: "form-control",
    type: "text",
    name: "ttt",
    placeholder: "0,00 RUB",
    required: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 62
    },
    __self: this
  }), money >= 1 ? "" : __jsx("div", {
    className: "error",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85
    },
    __self: this
  }, "\u0421\u0443\u043C\u043C\u0430 \u0434\u043E\u043B\u0436\u043D\u0430 \u0431\u044B\u0442\u044C \u0431\u043E\u043B\u044C\u0448\u0435 1 \u0440\u0443\u0431\u043B\u044F"), money <= 1000 ? "" : __jsx("div", {
    className: "error",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 86
    },
    __self: this
  }, "\u0421\u0443\u043C\u043C\u0430 \u043D\u0435 \u0434\u043E\u043B\u0436\u043D\u0430 \u043F\u0440\u0435\u0432\u044B\u0448\u0430\u0442\u044C 1000 \u0440\u0443\u0431\u043B\u0435\u0439")), __jsx("div", {
    className: "mt-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89
    },
    __self: this
  }, __jsx("div", {
    className: "btn btn-outline-primary",
    onClick: () => payPhone(),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 90
    },
    __self: this
  }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C")))));
} // Payment_view.isValid = function(){
//     debugger;
//     let valid = this.refs['myDateText'].isValid();
// }

/* harmony default export */ __webpack_exports__["default"] = (Payment_view);

/***/ })

})
//# sourceMappingURL=payment.js.cee8c422e4669da6d248.hot-update.js.map