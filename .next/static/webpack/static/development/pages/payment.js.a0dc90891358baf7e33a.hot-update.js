webpackHotUpdate("static\\development\\pages\\payment.js",{

/***/ "./src/payment/Payment.tsx":
/*!*********************************!*\
  !*** ./src/payment/Payment.tsx ***!
  \*********************************/
/*! exports provided: Payment_view, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Payment_view", function() { return Payment_view; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _payment_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./payment.scss */ "./src/payment/payment.scss");
/* harmony import */ var _payment_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_payment_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_masked_text__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-masked-text */ "./node_modules/react-masked-text/dist/text-input-mask.js");
/* harmony import */ var react_masked_text__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_masked_text__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dist/next-server/lib/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "C:\\Users\\dmitr\\Documents\\TestWeb\\my-app\\src\\payment\\Payment.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




function Payment_view({
  name
}) {
  const {
    0: money,
    1: setMoney
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])();
  const {
    0: number,
    1: setNumber
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])();
  const {
    0: numberField,
    1: setnumberField
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])();
  const payPhone = next_dynamic__WEBPACK_IMPORTED_MODULE_3___default()(() => Promise.all(/*! import() */[__webpack_require__.e("styles"), __webpack_require__.e(1)]).then(__webpack_require__.bind(null, /*! ../Home */ "./src/Home.tsx")), {
    loading: () => __jsx("div", {
      __source: {
        fileName: _jsxFileName,
        lineNumber: 16
      },
      __self: this
    }, "loadig..."),
    loadableGenerated: {
      webpack: () => [/*require.resolve*/(/*! ../Home */ "./src/Home.tsx")],
      modules: ["../Home"]
    }
  });
  return __jsx("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }, __jsx("form", {
    method: "POST",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, __jsx("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    __self: this
  }, name)), __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, __jsx("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, "\u041D\u043E\u043C\u0435\u0440 \u0442\u0435\u043B\u0435\u0444\u043E\u043D\u0430"), __jsx(react_masked_text__WEBPACK_IMPORTED_MODULE_2___default.a, {
    ref: ref => setnumberField(ref),
    kind: 'cel-phone',
    options: {
      withDDD: true,
      dddMask: '9 (999) 999-99-99'
    },
    value: number,
    onChangeText: text => {
      if (text === "8") text = "";
      if (text.length === 1) text = "8 " + text;
      if (text.length < 18) setNumber(text);
    },
    placeholder: "8 (999) 999-99-99",
    className: "form-control",
    required: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }), __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }, numberField != null && numberField.isValid() ? "" : __jsx("div", {
    className: "error mb-2",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53
    },
    __self: this
  }, "\u041D\u0435 \u043F\u0440\u0430\u0432\u0438\u043B\u044C\u043D\u043E \u0432\u0432\u0435\u0434\u0435\u043D\u043D\u044B\u0439 \u043D\u043E\u043C\u0435\u0440"))), __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 56
    },
    __self: this
  }, __jsx("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }, "\u0421\u0443\u043C\u043C\u0430 \u043F\u043E\u043F\u043E\u043B\u043D\u0435\u043D\u0438\u044F"), __jsx(react_masked_text__WEBPACK_IMPORTED_MODULE_2___default.a, {
    kind: 'money',
    options: {
      maskedValue: 3,
      delimiter: ' ',
      unit: '',
      suffixUnit: 'RUB',
      zeroCents: true
    },
    value: money,
    onChangeText: text => {
      let index = text.indexOf('RUB') - 1;

      if (index <= 8) {
        setMoney(text.slice(0, index));
      }
    },
    className: "form-control",
    type: "text",
    name: "ttt",
    placeholder: "0,00 RUB",
    required: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 58
    },
    __self: this
  })), __jsx("div", {
    className: "mt-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: this
  }, __jsx("div", {
    className: "btn btn-outline-primary",
    onClick: () => payPhone(),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 83
    },
    __self: this
  }, "\u041E\u043F\u043B\u0430\u0442\u0438\u0442\u044C")))));
} // Payment_view.isValid = function(){
//     debugger;
//     let valid = this.refs['myDateText'].isValid();
// }

/* harmony default export */ __webpack_exports__["default"] = (Payment_view);

/***/ })

})
//# sourceMappingURL=payment.js.a0dc90891358baf7e33a.hot-update.js.map