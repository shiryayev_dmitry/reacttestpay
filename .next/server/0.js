exports.ids = [0];
exports.modules = {

/***/ "./src/Home.tsx":
/*!**********************!*\
  !*** ./src/Home.tsx ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _header_Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header/Header */ "./src/header/Header.tsx");
/* harmony import */ var _main_Main__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./main/Main */ "./src/main/Main.tsx");
var _jsxFileName = "C:\\Users\\dmitr\\Documents\\TestWeb\\my-app\\src\\Home.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




function Home() {
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, __jsx(_header_Header__WEBPACK_IMPORTED_MODULE_1__["default"], {
    text: "\u0412\u044B\u0431\u0440\u0430\u0442\u044C \u043E\u043F\u0435\u0440\u0430\u0442\u043E\u0440\u0430",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }), __jsx(_main_Main__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }));
}

/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ "./src/main/Main.tsx":
/*!***************************!*\
  !*** ./src/main/Main.tsx ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Operators */ "./src/main/Operators.tsx");
var _jsxFileName = "C:\\Users\\dmitr\\Documents\\TestWeb\\my-app\\src\\main\\Main.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const Main = () => __jsx("div", {
  className: "container",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 5
  },
  __self: undefined
}, __jsx("div", {
  className: "row",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 6
  },
  __self: undefined
}, __jsx("div", {
  className: "col-md-4",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 7
  },
  __self: undefined
}, __jsx(_Operators__WEBPACK_IMPORTED_MODULE_1__["default"], {
  name: "mts",
  operator: "\u041C\u0422\u0421",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 8
  },
  __self: undefined
})), __jsx("div", {
  className: "col-md-4",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 10
  },
  __self: undefined
}, __jsx(_Operators__WEBPACK_IMPORTED_MODULE_1__["default"], {
  name: "mgf",
  operator: "\u041C\u0435\u0433\u0430\u0444\u043E\u043D",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 11
  },
  __self: undefined
})), __jsx("div", {
  className: "col-md-4",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 13
  },
  __self: undefined
}, __jsx(_Operators__WEBPACK_IMPORTED_MODULE_1__["default"], {
  name: "bil",
  operator: "\u0411\u0438\u043B\u0430\u0439\u043D",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 14
  },
  __self: undefined
}))));

/* harmony default export */ __webpack_exports__["default"] = (Main);

/***/ }),

/***/ "./src/main/Operators.tsx":
/*!********************************!*\
  !*** ./src/main/Operators.tsx ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _operator_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./operator.scss */ "./src/main/operator.scss");
/* harmony import */ var _operator_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_operator_scss__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "C:\\Users\\dmitr\\Documents\\TestWeb\\my-app\\src\\main\\Operators.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




function Operator(props) {
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, __jsx("div", {
    className: "transform-items mt-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, __jsx(next_link__WEBPACK_IMPORTED_MODULE_1___default.a, {
    as: '/payment/${name}',
    href: `/payment?name=${props.operator}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }, __jsx("div", {
    className: `operator ${props.name}`,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }))));
}

/* harmony default export */ __webpack_exports__["default"] = (Operator);

/***/ }),

/***/ "./src/main/operator.scss":
/*!********************************!*\
  !*** ./src/main/operator.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ })

};;
//# sourceMappingURL=0.js.map