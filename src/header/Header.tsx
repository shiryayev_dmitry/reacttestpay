
import Link from 'next/link';
import React from 'react';

interface IHeader{
    text:string,
    link?:boolean
}

function HeaderLink ({link}){
    if(link)
    {
        return(
            <div>
                <Link href="/index">Назад</Link>
            </div>
        )
    }
    else{
        return(
            <div></div>
        )
    }
}

function Header({text, link}:IHeader){
    return(
        <div>
            <div className="flex p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
                <HeaderLink link={link} />
                <div className="my-0 mr-md-auto font-weight-normal  d-flex justify-content-center">
                    {text}
                </div>
                
            </div>
        </div>
    );
}

export default Header;