import React from "react";
import Header from './header/Header';
import Main from './main/Main';

function Home()
{
    return(
        <div>
            <Header text="Выбрать оператора"/>
            <Main />
        </div>
    );
}

export default Home;