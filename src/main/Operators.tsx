import React from 'react';
import Link from 'next/link';
import './operator.scss';

export interface IOperator {
    name : string,
    operator: string
}

function Operator(props : IOperator){
    return(
        <div>
           <div className="transform-items mt-3">
                <Link as={'/payment/${name}'} href={`/payment?name=${props.operator}`}>
                    <div className={`operator ${props.name}`}>
                    </div>
                </Link> 
             </div>   
        </div> 
    );
}

export default Operator;