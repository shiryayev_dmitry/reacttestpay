import React from 'react';
import Operator from "./Operators";


const Main = () => (
    <div className="container">
        <div className="row">
            <div className="col-md-4">
                <Operator name="mts" operator="МТС" />
            </div>
            <div className="col-md-4">
                <Operator name="mgf" operator="Мегафон" />
            </div>
            <div className="col-md-4">
                <Operator name="bil" operator="Билайн" />
            </div>
        </div>
    </div>
)

export default Main;