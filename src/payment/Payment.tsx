import React, {useState} from 'react';
import './payment.scss';
import Router from 'next/router'

import MaskedInput from 'react-masked-text';

interface IPaymentView{
    name:string
}

export function Payment_view ({name} : IPaymentView){
    const [money, setMoney] = useState(1);
    const [number, setNumber] = useState();
    const [numberField, setnumberField] = useState();

    function payPhone(){
        if(numberField.isValid() && money>=1 && money<=1000)
        {
            if(Math.random() >= 0.5)
                Router.push('/index');
            else
                alert("Ошибка при отправке средств. Повторите позднее");
        }
        
    }

    return(
        <div className="container">
            <form method="POST">
                <div>
                    <h1>{name}</h1>
                </div> 
                <div>
                    <div>
                        <label>Номер телефона</label>
                        <MaskedInput 
                        ref={(ref)=> setnumberField(ref)}
                        kind={'cel-phone'}
                        options={          
                            {
                                withDDD:true,
                                dddMask:'9 (999) 999-99-99'
                            }
                        }
                        value={number}
                        onChangeText = {
                            text=>{
                                if(text === "8")
                                    text = "";

                                if(text.length === 1)
                                    text = "8 " + text;

                                if(text.length < 18)
                                    setNumber(text);
                            }
                        }
                        placeholder="8 (999) 999-99-99"
                        className="form-control" required/>
                        <div>
                            {numberField!=null && numberField.isValid()?"":(<div className="error mb-2">Не правильно введенный номер</div>)}
                        </div>
                    </div>
                    <div>
                    <label>Сумма пополнения</label>
                        <MaskedInput 
                        kind={'money'}
                        options={{ 
                            maskedValue:3,
                            delimiter:' ',
                            unit:'',
                            suffixUnit: 'RUB',
                            zeroCents:true }}

                        value={money}
                        onChangeText={text => {
                            let index = text.indexOf('RUB')-1;
                            if(index <= 8)
                            {
                                text = text.replace(" ","");
                                setMoney(parseFloat(text.slice(0, index)));
                            }                            
                        }}
                        className="form-control"
                        type="text"
                        name="ttt"
                        placeholder="0,00 RUB"
                        required
                        />
                        {money>=1?"":<div className="error">Сумма должна быть больше 1 рубля</div>}
                        {money<=1000?"":<div className="error">Сумма не должна превышать 1000 рублей</div>}

                    </div>
                    <div className="mt-3">
                        <div className="btn btn-outline-primary" 
                        onClick={()=>payPhone()}>
                            Оплатить
                        </div>
                    </div>
                </div>
            </form>
            
        </div>
    )
}


// Payment_view.isValid = function(){
//     debugger;
//     let valid = this.refs['myDateText'].isValid();
// }

export default Payment_view;