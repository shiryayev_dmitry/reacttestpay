import "../styles/styles.scss"
import React from 'react';
import Payment_view from '../src/payment/Payment'
import Header from "../src/header/Header";

//страница оплаты
const Payment = ({ url: { query : {name}}}) => (
    <div>
        
        <Header text="" link={true}/>
        <Payment_view name={name}/>
    </div>
)

export default Payment;