import "../styles/styles.scss"
import React from 'react';
import Home from '../src/Home';

//Главная страница
function App (){
    return(
        <div>
            <Home />
        </div>
    );
}

export default App;